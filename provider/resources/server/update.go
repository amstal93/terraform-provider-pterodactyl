package server

import (
	"context"
	"encoding/json"
	"terraform-provider-pterodactyl/provider/resources"
	"terraform-provider-pterodactyl/provider/structs"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/vicanso/go-axios"
)

func updateServer(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	providerCtx := m.(*structs.ProviderContext)
	detailsBody, _ := json.Marshal(map[string]interface{}{
		"name":        d.Get("name"),
		"user":        resources.GetUserId(providerCtx),
		"external_id": d.Get("external_id"),
		"description": d.Get("description"),
	})
	axios.Request(&axios.Config{
		Method:  "PATCH",
		BaseURL: providerCtx.Url,
		URL:     "/api/application/servers/" + d.Id() + "/details",
		Body:    detailsBody,
		Headers: resources.GetHeaders(providerCtx.ApiKeys.AppKey),
	})
	buildDetailsBody, _ := json.Marshal(map[string]interface{}{

		"allocation": d.Get("allocation_id"),
		"memory":     d.Get("limits.memory"),
		"swap":       d.Get("limits.swap"),
		"disk":       d.Get("limits.disk"),
		"io":         d.Get("limits.io"),
		"cpu":        d.Get("limits.cpu"),
		"feature_limits": map[string]interface{}{
			"databases": d.Get("feature_limits.databases"),
			"backups":   d.Get("feature_limits.backups"),
		},
	})

	axios.Request(&axios.Config{
		Method:  "PATCH",
		Body:    buildDetailsBody,
		BaseURL: providerCtx.Url,
		URL:     "/api/application/servers/" + d.Id() + "/build",
		Headers: resources.GetHeaders(providerCtx.ApiKeys.AppKey),
	})

	startupDetailsBody, _ := json.Marshal(map[string]interface{}{
		"startup":      d.Get("startup_command"),
		"environment":  d.Get("environment"),
		"egg":          d.Get("egg_id"),
		"image":        d.Get("docker_image"),
		"skip_scripts": false,
	})

	axios.Request(&axios.Config{
		Method:  "PATCH",
		Body:    startupDetailsBody,
		BaseURL: providerCtx.Url,
		URL:     "/api/application/servers/" + d.Id() + "/startup",
		Headers: resources.GetHeaders(providerCtx.ApiKeys.AppKey),
	})

	return diag.Diagnostics{}
}
