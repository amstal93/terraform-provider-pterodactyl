package resources

import (
	"encoding/json"
	"net/http"
	"terraform-provider-pterodactyl/provider/structs"

	"github.com/vicanso/go-axios"
)

func GetUserId(providerCtx *structs.ProviderContext) int {
	res, _ := axios.Request(&axios.Config{
		BaseURL: providerCtx.Url,
		URL:     "/api/client/account",
		Method:  "GET",
		Headers: GetHeaders(providerCtx.ApiKeys.ClientKey),
	})
	var response interface{}
	json.Unmarshal(res.Data, &response)
	return int(response.(map[string]interface{})["attributes"].(map[string]interface{})["id"].(float64))
}

func GetHeaders(apiKey string) http.Header {
	return http.Header{
		"Content-Type":  {"application/json"},
		"Accept":        {"Application/vnd.pterodactyl.v1+json"},
		"Authorization": {"Bearer " + apiKey},
	}
}
