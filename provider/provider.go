package provider

import (
	"context"
	"terraform-provider-pterodactyl/provider/resources/server"
	"terraform-provider-pterodactyl/provider/structs"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// Provider -
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"application_token": {
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				DefaultFunc: schema.EnvDefaultFunc("PTERO_APP_API_KEY", nil),
			},
			"client_token": {
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				DefaultFunc: schema.EnvDefaultFunc("PTERO_CLIENT_API_KEY", nil),
			},
			"url": {
				Type:      schema.TypeString,
				Required:  true,
				Sensitive: true,
			},
		},
		ConfigureContextFunc: providerConfigure,
		ResourcesMap: map[string]*schema.Resource{
			"pterodactyl_server": server.ResourceServer(),
		},
		DataSourcesMap: map[string]*schema.Resource{},
	}
}
func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	var diags diag.Diagnostics
	application_token := d.Get("application_token").(string)
	client_token := d.Get("client_token").(string)
	url := d.Get("url").(string)
	var context = &structs.ProviderContext{
		ApiKeys: structs.ApiKeys{
			ClientKey: client_token,
			AppKey:    application_token,
		},
		Url: url,
	}
	return context, diags
}
